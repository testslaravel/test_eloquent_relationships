<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use App\Models\Country;

class CountryController extends Controller
{
    public function index()
    {
        // TASK: load the relationship average of team size

        $countries = DB::table('countries')
            ->select('countries.name',DB::raw('AVG(teams.size) as teams_avg_size'))
            ->join('teams', 'countries.id', '=', 'teams.country_id')
            ->groupBy('countries.name')
            ->get();


        return view('countries.index', compact('countries'));
    }
}
