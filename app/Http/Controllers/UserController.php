<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users_all = User::all();

        $users = $users_all->filter(function ($user) {
            return $user->projects->isNotEmpty();
        });

        return view('users.index', compact('users'));
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }
}
