<ul>
    @foreach ($tasks as $task)
        @if(!empty($task->user))
        <li>{{ $task->name }} ({{ $task->user->name }})</li>
        @else
            <li>{{ $task->name }}</li>
        @endif
    @endforeach
</ul>
